package ma.octo.assignement.dto;

import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;

@Data @ToString
public class CashPaymentDto {
  private BigDecimal transferAmount;
  private Date dateExecution;
  private String issuerFullName;
  private String accountBeneficiaryNumber;
  private String bic;
  private String cashPaymentReason;
}

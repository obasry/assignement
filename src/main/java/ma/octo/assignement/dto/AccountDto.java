package ma.octo.assignement.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class AccountDto {
    private Long id;
    private String accountNumber;
    private String bic;
    private BigDecimal balance;
    private Long userId;
}

package ma.octo.assignement.dto;

import lombok.Data;

import java.util.Date;

@Data
public class AppUserDto {
    private Long id;
    private String userName;
    private String gender;
    private String lastName;
    private String firstName;
    private Date birthDate;
}

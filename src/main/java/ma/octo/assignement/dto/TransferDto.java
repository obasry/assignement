package ma.octo.assignement.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class TransferDto {
  private String issuerAccountNumber;
  private String beneficiaryAccountNumber;
  private String TransferReason;
  private BigDecimal transferAmount;
  private Date dateExecution;
}

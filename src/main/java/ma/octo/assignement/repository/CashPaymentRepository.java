package ma.octo.assignement.repository;

import ma.octo.assignement.domain.CashPayment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CashPaymentRepository extends JpaRepository<CashPayment, Long> {
}

package ma.octo.assignement.repository;

import ma.octo.assignement.domain.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepository extends JpaRepository<AppUser, Long> {
    AppUser findByUserName(String username);
}

package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.AppUser;
import ma.octo.assignement.dto.AppUserDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
    AppUserDto fromUserToUserDto(AppUser appUser);
    AppUser fromUserDtoToUser(AppUserDto appUserDto);
}

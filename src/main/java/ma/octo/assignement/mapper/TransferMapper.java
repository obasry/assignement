package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface TransferMapper {
    @Mapping(source = "issuerAccount.accountNumber", target = "issuerAccountNumber")
    @Mapping(source = "beneficiaryAccount.accountNumber", target = "beneficiaryAccountNumber")
    TransferDto fromTransferToTransferDto(Transfer transfer);
    Transfer fromTransferDtoToTransfer(TransferDto transferDto);
}

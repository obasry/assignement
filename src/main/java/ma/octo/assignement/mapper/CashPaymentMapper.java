package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.CashPayment;
import ma.octo.assignement.dto.CashPaymentDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface CashPaymentMapper {
    @Mapping(source = "accountBeneficiary.accountNumber", target = "accountBeneficiaryNumber")
    CashPaymentDto fromCashPaymentToCashPaymentDto(CashPayment cashPayment);
    CashPayment fromCashPaymentDtoToCashPayment(CashPaymentDto cashPaymentDto);
}

package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Account;
import ma.octo.assignement.dto.AccountDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface AccountMapper {
    @Mapping(source = "appUser.id", target = "userId")
    AccountDto fromAccountToAccountResponseDto(Account account);
}

package ma.octo.assignement;


import ma.octo.assignement.service.initData.BankInitService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


@SpringBootApplication
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class AssignementApplication {


    public static void main(String[] args) {
        SpringApplication.run(AssignementApplication.class, args);
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Profile("!test")
    CommandLineRunner start(BankInitService bankInitService) {
        return args -> {
            bankInitService.initUsers();
            bankInitService.initAccounts();
            bankInitService.initTransfers();
            bankInitService.initCashPayment();

        };
    }
}

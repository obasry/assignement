package ma.octo.assignement.service.initData;

public interface BankInitService {
    void initUsers();
    void initAccounts();
    void initTransfers();
    void initCashPayment();
}

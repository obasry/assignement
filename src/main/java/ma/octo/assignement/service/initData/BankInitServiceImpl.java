package ma.octo.assignement.service.initData;

import ma.octo.assignement.domain.*;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.repository.CashPaymentRepository;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.repository.UserRepository;
import ma.octo.assignement.service.AppRoleService;
import ma.octo.assignement.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Service
@Transactional
public class BankInitServiceImpl implements BankInitService {
    private final UserRepository userRepository;
    private final AccountRepository accountRepository;
    private final TransferRepository transferRepository;
    private final CashPaymentRepository cashPaymentRepository;
    private final AppRoleService appRoleService;
    private final UserService userService;

    public BankInitServiceImpl(UserRepository userRepository, AccountRepository accountRepository, TransferRepository transferRepository, CashPaymentRepository cashPaymentRepository, AppRoleService appRoleService, UserService userService) {
        this.userRepository = userRepository;
        this.accountRepository = accountRepository;
        this.transferRepository = transferRepository;
        this.cashPaymentRepository = cashPaymentRepository;
        this.appRoleService = appRoleService;
        this.userService = userService;
    }

    @Override
    public void initUsers() {
        appRoleService.addNewRole(new AppRole(null, "USER"));
        appRoleService.addNewRole(new AppRole(null, "ADMIN"));
        userService.addNewUser(new AppUser(null, "user1", "1234", new ArrayList<>(), "oussama", "basry", "male"));
        userService.addNewUser(new AppUser(null, "admin", "1234", new ArrayList<>(), "Ahmed", "Ahmed", "male"));
        userService.addNewUser(new AppUser(null, "user2", "1234", new ArrayList<>(), "Kawtar", "Kawtar", "female"));
        userService.addNewUser(new AppUser(null, "user3", "1234", new ArrayList<>(), "Walid", "Walid", "Male"));
        userService.addNewUser(new AppUser(null, "user4", "1234", new ArrayList<>(), "Naima", "Naima", "Female"));
        userService.addRoleToUser("user1", "USER");
        userService.addRoleToUser("admin", "USER");
        userService.addRoleToUser("user2", "USER");
        userService.addRoleToUser("user3", "USER");
        userService.addRoleToUser("user4", "USER");
        userService.addRoleToUser("admin", "ADMIN");
    }

    @Override
    public void initAccounts() {
        List<AppUser> appUsers = userRepository.findAll();
        int length = appUsers.size();
        final int[] accountNumber = {1};
        IntStream.range(0, length)
                .forEach(index ->{
                    Account account = new Account();

                    double random = 500 + Math.random() * (100000 - 500);
                    account.setAccountNumber(String.valueOf(accountNumber[0]));
                    account.setBic("RIB" + (index + 1));
                    account.setBalance(BigDecimal.valueOf(random));
                    account.setAppUser(appUsers.get(index));
                    accountRepository.save(account);
                    accountNumber[0]++;
                });
    }

    @Override
    public void initTransfers() {
        List<Account> accounts = accountRepository.findAll();
        Account issuerAccount = accounts.get(0);
        accounts.remove(issuerAccount);

        accounts.forEach(beneficiaryAccount -> {
            Transfer transfer = new Transfer();
            transfer.setTransferAmount(BigDecimal.TEN);
            transfer.setBeneficiaryAccount(beneficiaryAccount);
            transfer.setIssuerAccount(issuerAccount);
            transfer.setDateExecution(new Date());
            transfer.setTransferReason("Assignment "+ beneficiaryAccount.getId()+" 2021");
            transferRepository.save(transfer);
        });
    }

    @Override
    public void initCashPayment() {
        List<Account> accounts = accountRepository.findAll();
        accounts.forEach(beneficiary -> {
            Stream.of("Amine ARIA", "Khalid Namir", "Ayoub Motachakkir").forEach(fullName -> {
                CashPayment cashPayment = new CashPayment();
                cashPayment.setIssuerFullName(fullName);
                cashPayment.setCashPaymentReason("Reason cash payment");
                cashPayment.setDateExecution(new Date());
                cashPayment.setTransferAmount(BigDecimal.valueOf(2000));
                cashPayment.setAccountBeneficiary(beneficiary);
                cashPaymentRepository.save(cashPayment);
            });
        });

    }
}

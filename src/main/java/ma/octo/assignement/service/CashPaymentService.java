package ma.octo.assignement.service;


import ma.octo.assignement.dto.CashPaymentDto;

import java.util.List;

public interface CashPaymentService {
    List<CashPaymentDto> listCashPayments();
    CashPaymentDto createCashPayment(CashPaymentDto cashPaymentDto);
}

package ma.octo.assignement.service;

import ma.octo.assignement.domain.AppRole;
import ma.octo.assignement.repository.AppRoleRepository;
import org.springframework.stereotype.Service;

@Service
public class AppRoleServiceImpl implements AppRoleService {
    private AppRoleRepository appRoleRepository;

    public AppRoleServiceImpl(AppRoleRepository appRoleRepository) {
        this.appRoleRepository = appRoleRepository;
    }

    @Override
    public AppRole addNewRole(AppRole appRole) {
        return appRoleRepository.save(appRole);
    }
}

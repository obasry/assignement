package ma.octo.assignement.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import ma.octo.assignement.domain.AppRole;
import ma.octo.assignement.domain.AppUser;
import ma.octo.assignement.dto.AppUserDto;
import ma.octo.assignement.mapper.UserMapper;
import ma.octo.assignement.repository.AppRoleRepository;
import ma.octo.assignement.repository.UserRepository;
import ma.octo.assignement.security.JwtUtil;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final AppRoleRepository appRoleRepository;
    private final PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository userRepository, UserMapper userMapper, AppRoleRepository appRoleRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
        this.appRoleRepository = appRoleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public List<AppUserDto> listUsers() {
        List<AppUser> appUsers = userRepository.findAll();

        if (CollectionUtils.isEmpty(appUsers)) {
            return null;
        }
        List<AppUserDto> appUserDtoList = appUsers.stream().map(user ->
                userMapper.fromUserToUserDto(user)).collect(Collectors.toList());
        return appUserDtoList;
    }

    @Override
    public AppUser loadUserByUsername(String username) {
        return userRepository.findByUserName(username);
    }

    @Override
    public void addRoleToUser(String username, String roleName) {
        AppUser appUser = userRepository.findByUserName(username);
        AppRole appRole = appRoleRepository.findByRoleName(roleName);
        appUser.getAppRoles().add(appRole);
    }

    @Override
    public AppUserDto addNewUser(AppUser appUser) {
        appUser.setPassword(passwordEncoder.encode(appUser.getPassword()));
        return userMapper.fromUserToUserDto(userRepository.save(appUser));
    }

    @Override
    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String authToken = request.getHeader(JwtUtil.AUTH_HEADER);
        if (authToken != null && authToken.startsWith(JwtUtil.PREFIX)) {
            try {
                String jwtRefresh = authToken.substring(JwtUtil.PREFIX.length());
                Algorithm algorithm = Algorithm.HMAC256(JwtUtil.SECRET_KEY);
                JWTVerifier jwtVerifier = JWT.require(algorithm).build();
                DecodedJWT decodedJWT = jwtVerifier.verify(jwtRefresh);
                String username = decodedJWT.getSubject();
                AppUser appUser = loadUserByUsername(username);

                String jwtAccessToken = JWT.create()
                        .withSubject(appUser.getUserName())
                        .withExpiresAt(new Date(System.currentTimeMillis() + JwtUtil.EXPIRE_ACCESS_TOKEN))
                        .withIssuer(request.getRequestURL().toString())
                        .withClaim("roles", appUser.getAppRoles().stream().map(role -> role.getRoleName()).collect(Collectors.toList()))
                        .sign(algorithm);

                Map<String, String> idToken = new HashMap<>();
                idToken.put("access-token", jwtAccessToken);
                idToken.put("refresh-token", jwtRefresh);
                response.setContentType("application/json");
                new ObjectMapper().writeValue(response.getOutputStream(), idToken);

            } catch (TokenExpiredException | IOException e) {
                throw e;
            }
        } else {
            throw new RuntimeException("refresh token required");
        }
    }
}

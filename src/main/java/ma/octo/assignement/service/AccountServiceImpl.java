package ma.octo.assignement.service;

import ma.octo.assignement.domain.Account;
import ma.octo.assignement.dto.AccountDto;
import ma.octo.assignement.mapper.AccountMapper;
import ma.octo.assignement.mapper.UserMapper;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.repository.AppRoleRepository;
import ma.octo.assignement.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {
    private final AccountRepository accountRepository;
    private final AccountMapper accountMapper;
    private final UserMapper userMapper;
    private final UserRepository userRepository;
    private final AppRoleRepository appRoleRepository;



    public AccountServiceImpl(AccountRepository accountRepository, AccountMapper accountMapper, UserMapper userMapper, UserRepository userRepository, AppRoleRepository appRoleRepository) {
        this.accountRepository = accountRepository;
        this.accountMapper = accountMapper;
        this.userMapper = userMapper;
        this.userRepository = userRepository;
        this.appRoleRepository = appRoleRepository;
    }

    @Override
    public List<AccountDto> listAccounts() {
        List<Account> accounts = accountRepository.findAll();

        if (CollectionUtils.isEmpty(accounts)) {
            return null;
        }
        List<AccountDto> accountDtoList = accounts.stream().map((account) ->
                accountMapper.fromAccountToAccountResponseDto(account)

        ).collect(Collectors.toList());
        return accountDtoList;
    }
}

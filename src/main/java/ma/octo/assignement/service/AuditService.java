package ma.octo.assignement.service;

public interface AuditService {
     void auditTransfer(String message);
     void auditCashPayment(String message);
}

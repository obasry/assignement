package ma.octo.assignement.service;


import ma.octo.assignement.dto.AccountDto;

import java.util.List;


public interface AccountService {
  List<AccountDto> listAccounts();
}

package ma.octo.assignement.service;

import ma.octo.assignement.domain.AppRole;

public interface AppRoleService {
    AppRole addNewRole(AppRole appRole);
}

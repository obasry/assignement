package ma.octo.assignement.service;

import ma.octo.assignement.aspect.transfer.CreateTransfer;
import ma.octo.assignement.domain.Account;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.repository.TransferRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class TransferServiceImpl implements TransferService {
    private final TransferRepository transferRepository;
    private final AccountRepository accountRepository;
    private final AuditService auditService;
    private final TransferMapper transferMapper;

    public TransferServiceImpl(TransferRepository transferRepository, AccountRepository accountRepository, AuditService auditService, TransferMapper transferMapper) {
        this.transferRepository = transferRepository;
        this.accountRepository = accountRepository;
        this.auditService = auditService;
        this.transferMapper = transferMapper;
    }

    @Override
    public List<TransferDto> listTransfers() {
        List<Transfer> transfers = transferRepository.findAll();
        if (CollectionUtils.isEmpty(transfers)) {
            return null;
        }
        List<TransferDto> transferDtoList = transfers.stream().map(transfer ->
                transferMapper.fromTransferToTransferDto(transfer)).collect(Collectors.toList());
        return transferDtoList;
    }


    @Override
    @CreateTransfer
    public TransferDto createTransaction(TransferDto transferDto){
        Account issuerAccount = accountRepository.findByAccountNumber(transferDto.getIssuerAccountNumber());
        Account beneficiaryAccount = accountRepository.findByAccountNumber(transferDto.getBeneficiaryAccountNumber());

        issuerAccount.setBalance(issuerAccount.getBalance().subtract(transferDto.getTransferAmount()));
        accountRepository.save(issuerAccount);

        beneficiaryAccount.setBalance(beneficiaryAccount.getBalance().add(transferDto.getTransferAmount()));
        accountRepository.save(beneficiaryAccount);

        Transfer transfer = transferMapper.fromTransferDtoToTransfer(transferDto);
        transfer.setBeneficiaryAccount(beneficiaryAccount);
        transfer.setIssuerAccount(issuerAccount);

        TransferDto transferDtoResponse = transferMapper.fromTransferToTransferDto(transferRepository.save(transfer));

        auditService.auditTransfer("Virement depuis " + transferDto.getIssuerAccountNumber() + " vers " + transferDto
                .getBeneficiaryAccountNumber() + " d'un montant de " + transferDto.getTransferAmount()
                .toString());

        return transferDtoResponse;
    }
}

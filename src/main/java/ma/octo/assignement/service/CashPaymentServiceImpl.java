package ma.octo.assignement.service;


import ma.octo.assignement.aspect.cashpayment.CreateCashPayment;
import ma.octo.assignement.domain.Account;
import ma.octo.assignement.domain.CashPayment;
import ma.octo.assignement.dto.CashPaymentDto;
import ma.octo.assignement.mapper.CashPaymentMapper;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.repository.CashPaymentRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CashPaymentServiceImpl implements CashPaymentService {
    private CashPaymentRepository cashPaymentRepository;
    private CashPaymentMapper cashPaymentMapper;
    private AccountRepository accountRepository;
    private AuditService auditService;

    public CashPaymentServiceImpl(CashPaymentRepository cashPaymentRepository, CashPaymentMapper cashPaymentMapper, AccountRepository accountRepository, AuditService auditService) {
        this.cashPaymentRepository = cashPaymentRepository;
        this.cashPaymentMapper = cashPaymentMapper;
        this.accountRepository = accountRepository;
        this.auditService = auditService;
    }

    @Override
    public List<CashPaymentDto> listCashPayments() {
        List<CashPayment> cashPayments = cashPaymentRepository.findAll();
        if (CollectionUtils.isEmpty(cashPayments)) {
            return null;
        }
        List<CashPaymentDto> cashPaymentDtoList = cashPayments.stream().map(cashPayment ->
                cashPaymentMapper.fromCashPaymentToCashPaymentDto(cashPayment)).collect(Collectors.toList());
        return cashPaymentDtoList;
    }

    @Override
    @CreateCashPayment
    public CashPaymentDto createCashPayment(CashPaymentDto cashPaymentDto) {

        Account beneficiaryAccount = accountRepository.findByBic(cashPaymentDto.getBic());

        beneficiaryAccount.setBalance(beneficiaryAccount.getBalance().add(cashPaymentDto.getTransferAmount()));
        accountRepository.save(beneficiaryAccount);

        CashPayment cashPayment = cashPaymentMapper.fromCashPaymentDtoToCashPayment(cashPaymentDto);
        cashPayment.setAccountBeneficiary(beneficiaryAccount);

        CashPayment cashPaymentResp = cashPaymentRepository.save(cashPayment);

        CashPaymentDto cashPaymentDtoResponse = cashPaymentMapper.fromCashPaymentToCashPaymentDto(cashPaymentResp);

        auditService.auditCashPayment("Virsement depuis " + cashPaymentDto.getIssuerFullName() + " vers " + cashPaymentDto
                .getAccountBeneficiaryNumber() + " d'un montant de " + cashPaymentDto.getTransferAmount()
                .toString());

        return cashPaymentDtoResponse;
    }
}

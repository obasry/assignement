package ma.octo.assignement.service;

import ma.octo.assignement.domain.AppUser;
import ma.octo.assignement.dto.AppUserDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


public interface UserService {
    List<AppUserDto> listUsers();

    AppUser loadUserByUsername(String username);

    void addRoleToUser(String username, String roleName);

    AppUserDto addNewUser(AppUser appUser);

    void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException;
}

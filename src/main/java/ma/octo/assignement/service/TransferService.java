package ma.octo.assignement.service;

import ma.octo.assignement.dto.TransferDto;

import java.util.List;

public interface TransferService {
    List<TransferDto> listTransfers();
    TransferDto createTransaction(TransferDto transferDto);
}

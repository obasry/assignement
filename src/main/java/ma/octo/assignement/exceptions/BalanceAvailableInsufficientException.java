package ma.octo.assignement.exceptions;

public class BalanceAvailableInsufficientException extends Exception {

  private static final long serialVersionUID = 1L;

  public BalanceAvailableInsufficientException() {
  }

  public BalanceAvailableInsufficientException(String message) {
    super(message);
  }
}

package ma.octo.assignement.exceptions;

public class AccountNotExistException extends RuntimeException {

  private static final long serialVersionUID = 7718828512143293558L;

  public AccountNotExistException() {
  }

  public AccountNotExistException(String message) {
    super(message);
  }
}

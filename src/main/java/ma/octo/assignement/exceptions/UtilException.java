package ma.octo.assignement.exceptions;

public class UtilException {
    public static final String EMPTY_AMOUNT_MESSAGE = "Empty Amount";
    public static final String EMPTY_REASON_MESSAGE = "Empty Reason";
    public static final String MINIMUM_AMOUNT_NOT_REACHED_MESSAGE = "Minimum transfer amount not reached";
    public static final String ISSUER_NOT_EXIST = "Issuer account not exist";
    public static final String BENEFICIARY_NOT_EXIST = "Beneficiary account not exist";
    public static final String  SAME_ACCOUNTS_MESSAGE = "beneficiary and issuer are same accounts";
    public static final String MAXIMUM_AMOUNT_EXCEEDED = "Maximum transfer amount exceeded";
    public static final String MAXIMUM_TRANSFER_AMOUNT_EXCEEDED = "Maximum transfer amount exceeded";
}

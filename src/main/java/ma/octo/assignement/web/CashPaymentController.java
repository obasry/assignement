package ma.octo.assignement.web;

import ma.octo.assignement.dto.CashPaymentDto;
import ma.octo.assignement.service.CashPaymentService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController(value = "cashPayment")
class CashPaymentController {

    private CashPaymentService cashPaymentService;

    public CashPaymentController(CashPaymentService cashPaymentService) {
        this.cashPaymentService = cashPaymentService;
    }

    @GetMapping("/cashPayments")
    List<CashPaymentDto> allTransfers() {
        return cashPaymentService.listCashPayments();
    }


    @PostMapping("/executeCashPayment")
    @ResponseStatus(HttpStatus.CREATED)
    public CashPaymentDto createCashPayment(@RequestBody CashPaymentDto cashPaymentDto) {
         return cashPaymentService.createCashPayment(cashPaymentDto);
    }

}

package ma.octo.assignement.web.common;

import ma.octo.assignement.exceptions.AccountNotExistException;
import ma.octo.assignement.exceptions.BalanceAvailableInsufficientException;
import ma.octo.assignement.exceptions.TransactionException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class ExceptionHandlingController {

    @ExceptionHandler(BalanceAvailableInsufficientException.class)
    public ResponseEntity<String> handleBalanceAvailableInsufficientException(BalanceAvailableInsufficientException ex, WebRequest request) {
        return new ResponseEntity<>(ex.getMessage(), null, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AccountNotExistException.class)
    public ResponseEntity<String> handleAccountNotExistException(AccountNotExistException ex, WebRequest request) {
        return new ResponseEntity<>(ex.getMessage(), null, HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler(TransactionException.class)
    public ResponseEntity<String> handleTransactionException(TransactionException ex, WebRequest request) {
        return new ResponseEntity<>(ex.getMessage(), null, HttpStatus.BAD_REQUEST);
    }
}

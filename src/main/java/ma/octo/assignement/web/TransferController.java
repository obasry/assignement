package ma.octo.assignement.web;


import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.AccountNotExistException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.TransferService;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController(value = "transfer")
class TransferController {

    private TransferService transferService;

    public TransferController(TransferService transferService) {
        this.transferService = transferService;
    }

    @GetMapping("/transfers")
    @PreAuthorize("hasAuthority('ADMIN')")
    List<TransferDto> allTransfers() {
        return transferService.listTransfers();
    }


    @PostMapping("/executeTransfer")
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAuthority('USER')")
    public TransferDto createTransaction(@RequestBody TransferDto transferDto) throws TransactionException, AccountNotExistException {
         return transferService.createTransaction(transferDto);
    }

}

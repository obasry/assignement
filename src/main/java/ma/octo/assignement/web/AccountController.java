package ma.octo.assignement.web;


import ma.octo.assignement.dto.AccountDto;
import ma.octo.assignement.service.AccountService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AccountController {
    private AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("accounts")
    List<AccountDto> loadAllAccounts() {
        return accountService.listAccounts();
    }

}

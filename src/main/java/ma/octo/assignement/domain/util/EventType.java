package ma.octo.assignement.domain.util;

public enum EventType {

    TRANSFER("virement"),
    CASH_PAYMENT("Versement d'argent");

    private String type;

    EventType(String type) {
        this.type = type;
    }
}

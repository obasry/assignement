package ma.octo.assignement.domain.util;

import lombok.Data;

@Data
public
class RoleUserForm{
    String username;
    String roleName;
}

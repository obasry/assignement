package ma.octo.assignement.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;


@Entity
@Table(name = "USER")
@Data @AllArgsConstructor @NoArgsConstructor @Builder
public class AppUser implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(length = 10, nullable = false, unique = true)
    private String userName;

    @Column(length = 10, nullable = false)
    private String gender;

    @Column(length = 60, nullable = false)
    private String lastName;

    @Column(length = 60, nullable = false)
    private String firstName;

    @Temporal(TemporalType.DATE)
    private Date birthDate;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;
    @ManyToMany(fetch = FetchType.EAGER)
    private Collection<AppRole> appRoles=new ArrayList<>();

    public AppUser(Long id, String userName,String password, Collection<AppRole> appRoles, String firstName, String lastName, String gender) {
        this.id = id;
        this.userName = userName;
        this.appRoles = appRoles;
        this.password = password;
        this.firstName=firstName;
        this.lastName = lastName;
        this.gender = gender;
    }
}

package ma.octo.assignement.domain;


import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "TRANSFER")
@Data @AllArgsConstructor @NoArgsConstructor @Builder @ToString
public class Transfer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(precision = 16, scale = 2, nullable = false)
    private BigDecimal TransferAmount;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateExecution;

    @ManyToOne
    private Account issuerAccount;

    @ManyToOne
    private Account beneficiaryAccount;

    @Column(length = 200)
    private String TransferReason;

}

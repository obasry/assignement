package ma.octo.assignement.domain;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "ACCOUNT")
@Data @ToString @NoArgsConstructor @AllArgsConstructor
@Builder
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(length = 16, unique = true)
    private String accountNumber;

    private String bic;

    @Column(precision = 16, scale = 2)
    private BigDecimal balance;

    @ManyToOne()
    @JoinColumn(name = "user_id")
    private AppUser appUser;
}

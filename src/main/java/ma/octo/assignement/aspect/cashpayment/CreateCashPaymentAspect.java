package ma.octo.assignement.aspect.cashpayment;


import ma.octo.assignement.domain.Account;
import ma.octo.assignement.dto.CashPaymentDto;
import ma.octo.assignement.exceptions.AccountNotExistException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.exceptions.UtilException;
import ma.octo.assignement.repository.AccountRepository;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;


@Component
@Aspect
@EnableAspectJAutoProxy
public class CreateCashPaymentAspect {
    private final AccountRepository accountRepository;

    public CreateCashPaymentAspect(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Before(value = "@annotation(ma.octo.assignement.aspect.cashpayment.CreateCashPayment)")
    public void log(JoinPoint joinPoint) throws AccountNotExistException, TransactionException {
        CashPaymentDto cashPaymentDto = (CashPaymentDto) joinPoint.getArgs()[0];
        Account beneficiaryAccount = accountRepository.findByBic(cashPaymentDto.getBic());

        if (beneficiaryAccount == null) {
            throw new AccountNotExistException(UtilException.BENEFICIARY_NOT_EXIST);
        } else if (cashPaymentDto.getTransferAmount() == null || cashPaymentDto.getTransferAmount().intValue() == 0) {
            throw new TransactionException(UtilException.EMPTY_AMOUNT_MESSAGE);
        } else if (cashPaymentDto.getCashPaymentReason().length() <= 0) {
            throw new TransactionException(UtilException.EMPTY_REASON_MESSAGE);
        } else if (cashPaymentDto.getTransferAmount().subtract(BigDecimal.valueOf(10)).compareTo(BigDecimal.valueOf(10)) < 0 ) {
            throw new TransactionException(UtilException.MINIMUM_AMOUNT_NOT_REACHED_MESSAGE);
        }
    }
}
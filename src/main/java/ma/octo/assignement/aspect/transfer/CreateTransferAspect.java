package ma.octo.assignement.aspect.transfer;


import ma.octo.assignement.domain.Account;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.AccountNotExistException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.exceptions.UtilException;
import ma.octo.assignement.repository.AccountRepository;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;


@Component
@Aspect
@EnableAspectJAutoProxy
public class CreateTransferAspect {
    public static final int MAXIMAL_AMOUNT = 10000;
    private final AccountRepository accountRepository;

    public CreateTransferAspect(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Before(value = "@annotation(ma.octo.assignement.aspect.transfer.CreateTransfer)")
    public void log(JoinPoint joinPoint) throws AccountNotExistException, TransactionException {
        TransferDto transferDto = (TransferDto) joinPoint.getArgs()[0];
        Account issuerAccount = accountRepository.findByAccountNumber(transferDto.getIssuerAccountNumber());
        Account beneficiaryAccount = accountRepository.findByAccountNumber(transferDto.getBeneficiaryAccountNumber());
        if (issuerAccount == null) {
            throw new AccountNotExistException(UtilException.ISSUER_NOT_EXIST);
        } else if (beneficiaryAccount == null) {
            throw new AccountNotExistException(UtilException.BENEFICIARY_NOT_EXIST);
        } else if(issuerAccount.getId()==beneficiaryAccount.getId()){
            throw new TransactionException(UtilException.SAME_ACCOUNTS_MESSAGE);
        } else if (transferDto.getTransferAmount() == null || transferDto.getTransferAmount().intValue() == 0) {
            throw new TransactionException(UtilException.EMPTY_AMOUNT_MESSAGE);
        } else if (transferDto.getTransferAmount().subtract(BigDecimal.valueOf(10)).compareTo(BigDecimal.valueOf(10)) < 0 ) {
            throw new TransactionException(UtilException.MINIMUM_AMOUNT_NOT_REACHED_MESSAGE);
        } else if (transferDto.getTransferReason().length() <= 0) {
            throw new TransactionException(UtilException.EMPTY_REASON_MESSAGE);
        } else if (transferDto.getTransferAmount().intValue() > MAXIMAL_AMOUNT) {
            throw new TransactionException(UtilException.MAXIMUM_AMOUNT_EXCEEDED);
        }
    }
}
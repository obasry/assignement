package ma.octo.assignement.service;

import ma.octo.assignement.domain.Account;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.AccountNotExistException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.exceptions.UtilException;
import ma.octo.assignement.repository.AccountRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@ActiveProfiles("test")
class TransferServiceImplTest {
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private TransferService transferService;

    @Test
    void createTransaction() {
        Account beneficiaryAccount = new Account();
        beneficiaryAccount.setBic("9845673R458987654");
        beneficiaryAccount.setAccountNumber("93457SK35749");
        beneficiaryAccount.setBalance(BigDecimal.valueOf(350000));
        accountRepository.save(beneficiaryAccount);

        Account issuerAccount = new Account();
        issuerAccount.setBic("254893E43647");
        issuerAccount.setAccountNumber("92746VRE353647");
        issuerAccount.setBalance(BigDecimal.valueOf(150000));
        accountRepository.save(issuerAccount);

        TransferDto transferDto = new TransferDto();
        transferDto.setTransferReason("ecommerce");
        transferDto.setTransferAmount(BigDecimal.valueOf(200));
        transferDto.setIssuerAccountNumber(issuerAccount.getAccountNumber());
        transferDto.setDateExecution(new Date());
        transferDto.setBeneficiaryAccountNumber(beneficiaryAccount.getAccountNumber());

        TransferDto response = transferService.createTransaction(transferDto);
        Assertions.assertThat(response.getTransferAmount()).isEqualTo(BigDecimal.valueOf(200));
    }

    @Test
    void createTransactionWithNoAmount() throws TransactionException {
        Account beneficiaryAccount = new Account();
        beneficiaryAccount.setBic("9845673R458987654");
        beneficiaryAccount.setAccountNumber("9345793SK35749");
        beneficiaryAccount.setBalance(BigDecimal.valueOf(350000));
        accountRepository.save(beneficiaryAccount);

        Account issuerAccount = new Account();
        issuerAccount.setBic("2986449304SE43647");
        issuerAccount.setAccountNumber("92746VHD7873647");
        issuerAccount.setBalance(BigDecimal.valueOf(10000));
        accountRepository.save(issuerAccount);

        TransferDto transferDto = new TransferDto();
        transferDto.setTransferReason("ecommerce-2");
        transferDto.setTransferAmount(null);
        transferDto.setIssuerAccountNumber(issuerAccount.getAccountNumber());
        transferDto.setDateExecution(new Date());
        transferDto.setBeneficiaryAccountNumber(beneficiaryAccount.getAccountNumber());

        TransactionException exceptionAmountNull = assertThrows(TransactionException.class, () -> {
            transferService.createTransaction(transferDto);
        });

       TransactionException exceptionAmountZero = assertThrows(TransactionException.class, () -> {
            transferDto.setTransferAmount(BigDecimal.valueOf(0));
            transferService.createTransaction(transferDto);
        });

        String expectedMessage = UtilException.EMPTY_AMOUNT_MESSAGE;
        String actualMessageForNull = exceptionAmountNull.getMessage();
        String actualMessageForZero = exceptionAmountZero.getMessage();

        assertTrue(actualMessageForNull.contains(expectedMessage));
        assertTrue(actualMessageForZero.contains(expectedMessage));
    }


    @Test
    void createTransactionWithoutReason() throws TransactionException {
        Account beneficiaryAccount = new Account();
        beneficiaryAccount.setBic("98456J87654");
        beneficiaryAccount.setAccountNumber("9345OI8875749");
        beneficiaryAccount.setBalance(BigDecimal.valueOf(350000));
        accountRepository.save(beneficiaryAccount);

        Account issuerAccount = new Account();
        issuerAccount.setBic("298REJ43647");
        issuerAccount.setAccountNumber("9274LKI7873647");
        issuerAccount.setBalance(BigDecimal.valueOf(10000));
        accountRepository.save(issuerAccount);

        TransferDto transferDto = new TransferDto();
        transferDto.setTransferReason("");
        transferDto.setTransferAmount(BigDecimal.valueOf(1200));
        transferDto.setIssuerAccountNumber(issuerAccount.getAccountNumber());
        transferDto.setDateExecution(new Date());
        transferDto.setBeneficiaryAccountNumber(beneficiaryAccount.getAccountNumber());

        TransactionException exceptionNoReason= assertThrows(TransactionException.class, () -> {
            transferService.createTransaction(transferDto);
        });

        String expectedMessage = UtilException.EMPTY_REASON_MESSAGE;
        String actualMessageNoReason= exceptionNoReason.getMessage();

        assertTrue(actualMessageNoReason.contains(expectedMessage));
    }

    @Test
    void createTransactionAmountNotReached() throws TransactionException {
        Account beneficiaryAccount = new Account();
        beneficiaryAccount.setBic("98456J876864354");
        beneficiaryAccount.setAccountNumber("9347I8875749");
        beneficiaryAccount.setBalance(BigDecimal.valueOf(37000));
        accountRepository.save(beneficiaryAccount);

        Account issuerAccount = new Account();
        issuerAccount.setBic("298REJ43735H647");
        issuerAccount.setAccountNumber("9274573647");
        issuerAccount.setBalance(BigDecimal.valueOf(10000));
        accountRepository.save(issuerAccount);

        TransferDto transferDto = new TransferDto();
        transferDto.setTransferReason("E-COM");
        transferDto.setTransferAmount(BigDecimal.valueOf(3));
        transferDto.setIssuerAccountNumber(issuerAccount.getAccountNumber());
        transferDto.setDateExecution(new Date());
        transferDto.setBeneficiaryAccountNumber(beneficiaryAccount.getAccountNumber());

        TransactionException exceptionNoReason= assertThrows(TransactionException.class, () -> {
            transferService.createTransaction(transferDto);
        });

        String expectedMessage = UtilException.MINIMUM_AMOUNT_NOT_REACHED_MESSAGE;
        String actualMessageAmountNotReached= exceptionNoReason.getMessage();

        assertTrue(actualMessageAmountNotReached.contains(expectedMessage));
    }
    @Test
    void createTransactionAccountNotExist() throws TransactionException {
        Account beneficiaryAccount = new Account();
        beneficiaryAccount.setBic("98456J876864354");
        beneficiaryAccount.setAccountNumber("38947494T363");
        beneficiaryAccount.setBalance(BigDecimal.valueOf(37000));
        accountRepository.save(beneficiaryAccount);

        Account issuerAccount = new Account();
        issuerAccount.setBic("298735H647");
        issuerAccount.setAccountNumber("927SDF73647");
        issuerAccount.setBalance(BigDecimal.valueOf(10000));
        accountRepository.save(issuerAccount);


        TransferDto transferDto = new TransferDto();
        transferDto.setTransferReason("E-COM");
        transferDto.setTransferAmount(BigDecimal.valueOf(3));
        transferDto.setIssuerAccountNumber("63736");
        transferDto.setDateExecution(new Date());
        transferDto.setBeneficiaryAccountNumber(beneficiaryAccount.getAccountNumber());

        AccountNotExistException exceptionIssuerNotExist= assertThrows(AccountNotExistException.class, () -> {
            transferService.createTransaction(transferDto);
        });

        transferDto.setBeneficiaryAccountNumber("defvfvfrv");
        transferDto.setIssuerAccountNumber(issuerAccount.getAccountNumber());

        AccountNotExistException exceptionBeneficiaryNotExist= assertThrows(AccountNotExistException.class, () -> {
            transferService.createTransaction(transferDto);
        });

        String expectedMessageIssuer = UtilException.ISSUER_NOT_EXIST;
        String actualMessageIssuerNotExist= exceptionIssuerNotExist.getMessage();
        assertTrue(actualMessageIssuerNotExist.contains(expectedMessageIssuer));

        String expectedMessageBeneficiary = UtilException.BENEFICIARY_NOT_EXIST;
        String actualMessageBeneficiaryNotExist= exceptionBeneficiaryNotExist.getMessage();
        assertTrue(actualMessageBeneficiaryNotExist.contains(expectedMessageBeneficiary));
    }

}
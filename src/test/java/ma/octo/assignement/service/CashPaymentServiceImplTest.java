package ma.octo.assignement.service;

import ma.octo.assignement.domain.Account;
import ma.octo.assignement.dto.CashPaymentDto;
import ma.octo.assignement.exceptions.AccountNotExistException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.exceptions.UtilException;
import ma.octo.assignement.repository.AccountRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@ActiveProfiles("test")
class CashPaymentServiceImplTest {
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    CashPaymentService cashPaymentService;

    @Test
    void createCashPayment() {
        Account account = new Account();
        account.setBic("2345673R45678987654");
        account.setAccountNumber("234567YD4567");
        account.setBalance(BigDecimal.valueOf(30000));
        accountRepository.save(account);

        CashPaymentDto cashPaymentDto = new CashPaymentDto();
        cashPaymentDto.setCashPaymentReason("ecommerce");
        cashPaymentDto.setTransferAmount(BigDecimal.valueOf(200));
        cashPaymentDto.setIssuerFullName("Oussama basry");
        cashPaymentDto.setDateExecution(new Date());
        cashPaymentDto.setAccountBeneficiaryNumber("234567YD4567");
        cashPaymentDto.setBic(account.getBic());

        CashPaymentDto response = cashPaymentService.createCashPayment(cashPaymentDto);
        Assertions.assertThat(response.getIssuerFullName()).isEqualTo("Oussama basry");
    }

    @Test
    void createTransactionWithNoAmount() throws TransactionException {
        Account beneficiaryAccount = new Account();
        beneficiaryAccount.setBic("9845673RJ7654");
        beneficiaryAccount.setAccountNumber("9345793SK35749");
        beneficiaryAccount.setBalance(BigDecimal.valueOf(350000));
        accountRepository.save(beneficiaryAccount);

        CashPaymentDto cashPaymentDto = new CashPaymentDto();
        cashPaymentDto.setCashPaymentReason("ecommerce-2");
        cashPaymentDto.setTransferAmount(null);
        cashPaymentDto.setIssuerFullName("MARWA Ismail");
        cashPaymentDto.setDateExecution(new Date());
        cashPaymentDto.setAccountBeneficiaryNumber(beneficiaryAccount.getAccountNumber());
        cashPaymentDto.setBic(beneficiaryAccount.getBic());

        TransactionException exceptionAmountNull = assertThrows(TransactionException.class, () -> {
            cashPaymentService.createCashPayment(cashPaymentDto);
        });

        TransactionException exceptionAmountZero = assertThrows(TransactionException.class, () -> {
            cashPaymentDto.setTransferAmount(BigDecimal.valueOf(0));
            cashPaymentService.createCashPayment(cashPaymentDto);
        });

        String expectedMessage = UtilException.EMPTY_AMOUNT_MESSAGE;
        String actualMessageForNull = exceptionAmountNull.getMessage();
        String actualMessageForZero = exceptionAmountZero.getMessage();

        assertTrue(actualMessageForNull.contains(expectedMessage));
        assertTrue(actualMessageForZero.contains(expectedMessage));
    }


    @Test
    void createTransactionWithoutReason() throws TransactionException {
        Account beneficiaryAccount = new Account();
        beneficiaryAccount.setBic("98456J898677654");
        beneficiaryAccount.setAccountNumber("9345OI8875749");
        beneficiaryAccount.setBalance(BigDecimal.valueOf(350000));
        accountRepository.save(beneficiaryAccount);

        CashPaymentDto cashPaymentDto = new CashPaymentDto();
        cashPaymentDto.setCashPaymentReason("");
        cashPaymentDto.setTransferAmount(BigDecimal.valueOf(1200));
        cashPaymentDto.setIssuerFullName("Charles MAGNIZ");
        cashPaymentDto.setDateExecution(new Date());
        cashPaymentDto.setAccountBeneficiaryNumber(beneficiaryAccount.getAccountNumber());
        cashPaymentDto.setBic(beneficiaryAccount.getBic());

        TransactionException exceptionNoReason= assertThrows(TransactionException.class, () -> {
            cashPaymentService.createCashPayment(cashPaymentDto);
        });

        String expectedMessage = UtilException.EMPTY_REASON_MESSAGE;
        String actualMessageNoReason= exceptionNoReason.getMessage();

        assertTrue(actualMessageNoReason.contains(expectedMessage));
    }

    @Test
    void createTransactionAmountNotReached() throws TransactionException {
        Account beneficiaryAccount = new Account();
        beneficiaryAccount.setBic("98456J876864354");
        beneficiaryAccount.setAccountNumber("9347I8875749");
        beneficiaryAccount.setBalance(BigDecimal.valueOf(37000));
        accountRepository.save(beneficiaryAccount);

        CashPaymentDto cashPaymentDto = new CashPaymentDto();
        cashPaymentDto.setCashPaymentReason("E-COM");
        cashPaymentDto.setTransferAmount(BigDecimal.valueOf(3));
        cashPaymentDto.setIssuerFullName("Joly MARON");
        cashPaymentDto.setDateExecution(new Date());
        cashPaymentDto.setAccountBeneficiaryNumber(beneficiaryAccount.getAccountNumber());
        cashPaymentDto.setBic(beneficiaryAccount.getBic());

        TransactionException exceptionNoReason= assertThrows(TransactionException.class, () -> {
            cashPaymentService.createCashPayment(cashPaymentDto);
        });

        String expectedMessage = UtilException.MINIMUM_AMOUNT_NOT_REACHED_MESSAGE;
        String actualMessageAmountNotReached= exceptionNoReason.getMessage();

        assertTrue(actualMessageAmountNotReached.contains(expectedMessage));
    }
    @Test
    void createTransactionAccountNotExist() throws TransactionException {
        CashPaymentDto cashPaymentDto = new CashPaymentDto();
        cashPaymentDto.setCashPaymentReason("E-COM");
        cashPaymentDto.setTransferAmount(BigDecimal.valueOf(350));
        cashPaymentDto.setIssuerFullName("Ahmed AB");
        cashPaymentDto.setDateExecution(new Date());
        cashPaymentDto.setAccountBeneficiaryNumber("test");

        AccountNotExistException exceptionBeneficiaryNotExist= assertThrows(AccountNotExistException.class, () -> {
            cashPaymentService.createCashPayment(cashPaymentDto);
        });

        String expectedMessageBeneficiary = UtilException.BENEFICIARY_NOT_EXIST;
        String actualMessageBeneficiaryNotExist= exceptionBeneficiaryNotExist.getMessage();
        assertTrue(actualMessageBeneficiaryNotExist.contains(expectedMessageBeneficiary));
    }
}
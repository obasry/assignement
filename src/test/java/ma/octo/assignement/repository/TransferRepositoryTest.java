package ma.octo.assignement.repository;


import ma.octo.assignement.domain.Account;
import ma.octo.assignement.domain.AppUser;
import ma.octo.assignement.domain.Transfer;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;



@ExtendWith(SpringExtension.class)
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@ActiveProfiles("test")
public class TransferRepositoryTest {

    @Autowired
    private TransferRepository transferRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private UserRepository userRepository;

    @Test
    public void save() {
        AppUser appUser1 = AppUser.builder()
                .userName("oussama")
                .firstName("oussama")
                .lastName("basry")
                .birthDate(new Date())
                .gender("Male")
                .build();
        userRepository.save(appUser1);

        AppUser appUser2 = AppUser.builder()
                .userName("ahmed")
                .firstName("Ahmed")
                .lastName("RAWI")
                .birthDate(new Date())
                .gender("Male")
                .build();

        userRepository.save(appUser2);

        Account account1 = Account.builder()
                .accountNumber("1234678")
                .bic("23456789")
                .balance(BigDecimal.valueOf(10000))
                .appUser(appUser1)
                .build();
        accountRepository.save(account1);
        Account account2 = Account.builder()
                .accountNumber("873456789")
                .bic("54746T4673483")
                .balance(BigDecimal.valueOf(2000))
                .appUser(appUser2)
                .build();
        accountRepository.save(account2);
        Transfer transfer = Transfer.builder()
                .TransferReason("ecom")
                .TransferAmount(BigDecimal.valueOf(3000))
                .beneficiaryAccount(account1)
                .issuerAccount(account2)
                .dateExecution(new Date())
                .build();
        transferRepository.save(transfer);
        Assertions.assertThat(transfer.getId()).isGreaterThan(0);
    }



    @Test
    @Order(1)
    public void findOne() {
        Transfer transfer = new Transfer();
        transfer.setTransferAmount(BigDecimal.valueOf(1000));
        transferRepository.save(transfer);
        Transfer transfer1 = transferRepository.findById(1L).get();
        Assertions.assertThat(transfer1.getId()).isEqualTo(1L);
    }

    @Test
    public void findAll() {
        Transfer transfer = new Transfer();
        transfer.setTransferAmount(BigDecimal.valueOf(1000));
        transferRepository.save(transfer);

        Transfer transfer1 = new Transfer();
        transfer1.setTransferAmount(BigDecimal.valueOf(1000));
        transferRepository.save(transfer1);

        List<Transfer> transfers = transferRepository.findAll();
        Assertions.assertThat(transfers.size()).isGreaterThan(0);
    }

    @Test
    public void delete() {
        Transfer transfer = new Transfer();
        transfer.setTransferAmount(BigDecimal.valueOf(1000));

        Transfer savedTransfer = transferRepository.save(transfer);

        transferRepository.delete(savedTransfer);

        Optional<Transfer> deletedTransfer = transferRepository.findById(savedTransfer.getId());

        Assertions.assertThat(deletedTransfer).isNotPresent();
    }

}
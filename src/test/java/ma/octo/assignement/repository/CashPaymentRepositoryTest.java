package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Account;
import ma.octo.assignement.domain.AppUser;
import ma.octo.assignement.domain.CashPayment;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@ActiveProfiles("test")
public class CashPaymentRepositoryTest {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private  AccountRepository accountRepository;
    @Autowired
    private CashPaymentRepository cashPaymentRepository;
    @Test
    public void save() {
        AppUser appUser = AppUser.builder()
                .userName("oussama")
                .firstName("oussama")
                .lastName("basry")
                .birthDate(new Date())
                .gender("Male")
                .build();
        userRepository.save(appUser);

        Account account = Account.builder()
                .accountNumber("1234678")
                .bic("23456789")
                .balance(BigDecimal.valueOf(10000))
                .appUser(appUser)
                .build();
        accountRepository.save(account);

        CashPayment cashPayment = CashPayment.builder()
                .cashPaymentReason("ecom")
                .transferAmount(BigDecimal.valueOf(3000))
                .accountBeneficiary(account)
                .issuerFullName("Amine Raji")
                .dateExecution(new Date())
                .build();
        cashPaymentRepository.save(cashPayment);
        Assertions.assertThat(cashPayment.getId()).isGreaterThan(0);
    }



    @Test
    @Order(1)
    @Rollback(value = false)
    public void findOne() {
        CashPayment cashPayment = new CashPayment();
        cashPayment.setTransferAmount(BigDecimal.valueOf(1000));
        cashPaymentRepository.save(cashPayment);
        CashPayment cashPayment1 = cashPaymentRepository.findById(1L).get();
        Assertions.assertThat(cashPayment1.getId()).isEqualTo(1L);
    }

    @Test
    public void findAll() {
        CashPayment cashPayment = new CashPayment();
        cashPayment.setTransferAmount(BigDecimal.valueOf(1000));
        cashPaymentRepository.save(cashPayment);

        CashPayment cashPayment1 = new CashPayment();
        cashPayment1.setTransferAmount(BigDecimal.valueOf(1000));
        cashPaymentRepository.save(cashPayment1);

        List<CashPayment> cashPayments = cashPaymentRepository.findAll();
        Assertions.assertThat(cashPayments.size()).isGreaterThan(0);
    }

    @Test
    public void delete() {
        CashPayment cashPayment = new CashPayment();
        cashPayment.setTransferAmount(BigDecimal.valueOf(1000));

        CashPayment savedCashPayment = cashPaymentRepository.save(cashPayment);

        cashPaymentRepository.delete(savedCashPayment);

        Optional<CashPayment> deletedCashPayment = cashPaymentRepository.findById(savedCashPayment.getId());

        Assertions.assertThat(deletedCashPayment).isNotPresent();
    }

}

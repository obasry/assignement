package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Account;
import ma.octo.assignement.domain.AppRole;
import ma.octo.assignement.domain.AppUser;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@ActiveProfiles("test")
public class AccountRepositoryTest {
    @Autowired
    private AppRoleRepository appRoleRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AccountRepository accountRepository;


    @Test
    @Order(1)
    public void findOne() {
        Account account = new Account();
        account.setAccountNumber("132324f43544656");
        account.setBic("217327836283T3629634294JS323");
        account.setBalance(BigDecimal.valueOf(40000));
        accountRepository.save(account);

        Account testAccount = accountRepository.findById(1L).get();
        Assertions.assertThat(testAccount.getId()).isEqualTo(1L);
    }

    @Test
    public void save() {
        AppRole appRole = new AppRole();
        appRole.setRoleName("ADMIN");
        appRoleRepository.save(appRole);

        AppUser appUser = new AppUser();
        appUser.setBirthDate(new Date());
        appUser.setFirstName("Mohammed");
        appUser.setLastName("Omari");
        appUser.setUserName("amad23");
        appUser.setGender("MALE");
        appUser.setPassword("nfceicben56563735@");
        appUser.setAppRoles(Arrays.asList(appRole));
        userRepository.save(appUser);

        Account account = new Account();
        account.setAccountNumber("12345678987654");
        account.setBalance(BigDecimal.valueOf(30000));
        account.setBic("1243567489T24536748");
        account.setAppUser(appUser);
        accountRepository.save(account);
        Assertions.assertThat(account.getId()).isGreaterThan(0);
    }

    @Test
    public void findAll() {
        Account account = new Account();
        account.setAccountNumber("132344656");
        account.setBic("21732783TD963429423");
        account.setBalance(BigDecimal.valueOf(40000));
        accountRepository.save(account);

        Account account2 = new Account();
        account2.setAccountNumber("358947132324f656");
        account2.setBic("89746H65564743");
        account2.setBalance(BigDecimal.valueOf(60000));
        accountRepository.save(account2);

        List<Account> accounts = accountRepository.findAll();
        Assertions.assertThat(accounts.size()).isGreaterThan(0);
    }

    @Test
    public void delete() {
        Account account = new Account();
        account.setAccountNumber("85743678T5467389");
        account.setBalance(BigDecimal.valueOf(1000));
        account.setBic("9743526789034T53678290437474");
        accountRepository.save(account);

        accountRepository.delete(account);

        Optional<Account> deletedAccount = accountRepository.findById(account.getId());

        Assertions.assertThat(deletedAccount).isNotPresent();
    }
}

package ma.octo.assignement.repository;


import ma.octo.assignement.domain.AppRole;
import ma.octo.assignement.domain.AppUser;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@ActiveProfiles("test")
public class UserRepositoryTest {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AppRoleRepository appRoleRepository;

    @Test
    public void save() {
        AppRole appRole =new AppRole(null, "USER");
        appRoleRepository.save(appRole);

        AppUser appUser = new AppUser();
        appUser.setUserName("AB123");
        appUser.setPassword("decndnn347347@BFBD");
        appUser.setGender("FEMALE");
        appUser.setFirstName("Mohammed");
        appUser.setLastName("AMIR");
        appUser.setBirthDate(new Date());
        appUser.setAppRoles(Arrays.asList());
        AppUser savedUser = userRepository.save(appUser);
        Assertions.assertThat(savedUser.getId()).isGreaterThan(0);
    }



    @Test
    @Order(1)
    public void findOne() {
        AppUser appUser = new AppUser();
        appUser.setUserName("username7");
        appUser.setFirstName("Karim");
        appUser.setLastName("ZIDAN");
        appUser.setGender("MALE");
        userRepository.save(appUser);
        AppUser appUser1 = userRepository.findById(1L).get();
        Assertions.assertThat(appUser1.getId()).isEqualTo(1L);
    }

    @Test
    public void findAll() {
        AppUser appUser = new AppUser();
        appUser.setUserName("username67");
        appUser.setFirstName("Karim");
        appUser.setLastName("ZIDAN");
        appUser.setGender("MALE");
        userRepository.save(appUser);

        AppUser appUser2 = new AppUser();
        appUser2.setUserName("username22");
        appUser2.setFirstName("Nadia");
        appUser2.setLastName("SAID");
        appUser2.setGender("FEMALE");
        userRepository.save(appUser2);

        List<AppUser> appUsers = userRepository.findAll();
        Assertions.assertThat(appUsers.size()).isGreaterThan(0);
    }

    @Test
    public void delete() {
        AppUser appUser = new AppUser();
        appUser.setUserName("username10");
        appUser.setFirstName("Karim");
        appUser.setLastName("ZIDAN");
        appUser.setGender("MALE");
        userRepository.save(appUser);

        AppUser savedAppUser = userRepository.save(appUser);

        userRepository.delete(savedAppUser);

        Optional<AppUser> deletedAppUser = userRepository.findById(savedAppUser.getId());

        Assertions.assertThat(deletedAppUser).isNotPresent();
    }

}